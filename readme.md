这个项目是DiscuzQ2.x的延续......也许......

## 安装方法

服务器环境需求为： **PHP 7.2.5+** 和 **MySQL 5.7+**, 并且需要安装 [Composer](https://getcomposer.org/)。



```
composer install

cd resources/frame
npm install
npm run build
```

## 感谢

<p><a href="https://laravel.com/"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="200"></a></p>
<p><a href="https://symfony.com/"><img src="https://symfony.com/images/logos/header-logo.svg" width="200"></a></p>
<p><a href="https://getlaminas.org/"><img src="https://getlaminas.org/images/logo/laminas-foundation-rgb.svg" width="200"></a></p>

[FastRoute](https://github.com/nikic/FastRoute)

[Guzzle](http://guzzlephp.org/)

[thephpleague](https://thephpleague.com/) 

[s9etextformatter](https://s9etextformatter.readthedocs.io/)

[overtrue](https://overtrue.me/)

[intervention](http://image.intervention.io/)

[monolog](https://github.com/Seldaek/monolog)

[whoops](https://github.com/filp/whoops)

[vue](https://vuejs.org/)

[Vant](https://youzan.github.io/vant/#/zh-CN/)

[element-ui](https://element.eleme.cn/#/zh-CN)


